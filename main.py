import os
from camera import Camera
os.environ["IF_CPP_MIN_LOG_LEVEL"] = "3"
from nn import NeuroNetwork


cam = Camera(
    user="user", 
    password="password",
    ip="IP",
    port=554)
neuro = NeuroNetwork()
#image = cam.save()
#neuro.object_detection_on_a_image("frame.jpg", "model/mask_rcnn_coco.h5")
ret, frame = cam.read()
neuro.object_detection_on_a_frame(frame, "model/mask_rcnn_coco.h5")