from typing import Any
from pixellib.instance import instance_segmentation

class NeuroNetwork:

    def __init__(self):
        pass

    def object_detection_on_a_frame(self, frame: Any, model_path: str):
        segment_frame = instance_segmentation()
        segment_frame.load_model(model_path)
        #segment_class = segment_frame.select_target_classes(car=True)

        result = segment_frame.segmentFrame(
            frame=frame,
            #segment_target_classes=segment_class,
            output_image_name="images/out_image.jpg", show_bboxes=True)
        
        return result

    def object_detection_on_a_image(self, image: Any, model_path: str):
        segment_frame = instance_segmentation()
        segment_frame.load_model(model_path)
        #segment_class = segment_frame.select_target_classes(person=True)

        result = segment_frame.segmentImage(
            image_path=image,
            #segment_target_classes=segment_class,
            output_image_name="images/out_image.jpg", show_bboxes=True)
        
        return result