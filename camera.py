import cv2

class Camera:

    def __init__(self, user: str = None, password: str = None, ip: str = None, port: int = None):
        self.user = user
        self.password = password
        self.ip = ip
        self.port = port

    def read(self, url: str = None):

        """
        
        : Читаем из ссылки информацию о камере 
        
        """

        if not url:
            url = f"rtsp://{self.user}:{self.password}@{self.ip}:{self.port}/cam/realmonitor?channel=1&subtype=0"
        self.capture = cv2.VideoCapture(url)
        return self.capture.read()

    def save(self):
        frame = self.read()[1]
        #frame = cv2.resize(frame, (1200, 800))
        cv2.imwrite("images/frame.jpg", frame)
        return "images/frame.jpg"

    def show(self):

        """
        
        : Получаем картинку с камеры и выводим в отдельное окно 
        
        """

        self.ret, self.frame = self.read()
        cv2.imshow("Camera", self.frame)

    def destroy_all_windows(self):

        """
        
        : Удаление окна с картинкой камеры 
        
        """

        cv2.destroyAllWindows()

    def wait_key(self, key: str = "q"):

        """
        
        : Проверка на нажатие клавиши 
        
        """

        if cv2.waitKey(1) & 0xFF == ord(key):
            return True
